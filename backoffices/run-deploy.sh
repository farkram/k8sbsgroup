# Create or apply deployment and services backend
kubectl apply -f ./backend-deploy/deploy-svc-auth.yaml \
 -f ./backend-deploy/deploy-svc-bank-transaction.yaml \
 -f ./backend-deploy/deploy-svc-bonus.yaml \
 -f ./backend-deploy/deploy-svc-manage-customers.yaml \
 -f ./backend-deploy/deploy-svc-manage-promotion.yaml \
 -f ./backend-deploy/deploy-svc-manage-user.yaml

# Create or apply deployment and services frontend
kubectl apply -f ./frontend/deploy-svc-backoffice.yaml